//Project Name
//Mike



#include <iostream>
#include <conio.h>

using namespace std;


	enum Rank { two = 2, three = 3, four = 4, five = 5, six = 6, seven = 7, eight = 8, nine = 9, ten = 10, J = 11, Q = 12 , K = 13, A = 14};
	enum Suit { Hearts, Diamonds, Clubs, Spades };

struct Card
{
	Rank rank;
	Suit suit;
	
};

void PrintCard(Card card)
{
	cout << "The ";

	switch (card.rank)
	{
	case two: cout << "two";
		break;
	case three:cout << "three";
		break;
	case four:cout << "four";
		break;
	case five:cout << "five";
		break;
	case six:cout << "six";
		break;
	case seven:cout << "seven";
		break;
	case eight:cout << "eight";
		break;
	case nine:cout << "nine";
		break;
	case ten:cout << "ten";
		break;
	case J:cout << "jack";
		break;
	case Q:cout << "queen";
		break;
	case K:cout << "king";
		break;
	case A:cout << "ace";
		break;
	}
	cout << " of ";
	switch (card.suit)
	{
	case Hearts: cout << "hearts\n";
		break;
	case Spades: cout << "spades\n";
		break;
	case Clubs: cout << "clubs\n";
		break;
	case Diamonds: cout << "diamonds\n";
		break;
	}
}

Card HighCard(Card card1, Card card2)
{
	if (card1.rank < card2.rank) cout << "Card 2 is higher";
	else cout << "Card 1 one is higher";
	return card1;
}

int main() 
{

	//Card twoOfHearts;
	//twoOfHearts.rank = two;
	//twoOfHearts.suit = Hearts;

	Card c1;
	c1.rank = nine;
	c1.suit = Diamonds;

	Card c2;
	c2.rank = A;
	c2.suit = Spades;

	PrintCard(c1);
	cout << "versus\n";
	PrintCard(c2);

	HighCard(c1, c2);

	_getch();
	return 0;
}



